<?php

declare(strict_types=1);

/*
 * This file is part of the littlesqx/aint-queue.
 *
 * (c) littlesqx <littlesqx@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled.
 */

namespace Collin\Queue\Console;

use Collin\Queue\Console\Command\QueueClearCommand;
use Collin\Queue\Console\Command\QueueDashboardCommand;
use Collin\Queue\Console\Command\QueueReloadFailedCommand;
use Collin\Queue\Console\Command\QueueStatusCommand;
use Collin\Queue\Console\Command\WorkerListenCommand;
use Collin\Queue\Console\Command\WorkerReloadCommand;
use Collin\Queue\Console\Command\WorkerRunCommand;
use Collin\Queue\Console\Command\WorkerStopCommand;
use Symfony\Component\Console\Application as BaseApplication;

final class Application extends BaseApplication
{
    public function __construct()
    {
        parent::__construct('AintQueue Console Tool');

        $this->addCommands([
            new WorkerRunCommand(),
            new WorkerListenCommand(),
            new QueueStatusCommand(),
            new QueueClearCommand(),
            new WorkerStopCommand(),
            new WorkerReloadCommand(),
            new QueueReloadFailedCommand(),
            new QueueDashboardCommand(),
        ]);
    }
}
